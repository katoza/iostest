//
//  main.m
//  iostest
//
//  Created by katobit on 12/2/2559 BE.
//  Copyright © 2559 katobit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
