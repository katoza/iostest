//
//  ViewController.m
//  iostest
//
//  Created by katobit on 12/2/2559 BE.
//  Copyright © 2559 katobit. All rights reserved.
//

#import "ViewController.h"
#import "CellData.h"
#import "MCPercentageDoughnutView.h"

#import <AFNetworking/AFNetworking.h>

@interface ViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tbMain;
@property (strong, nonatomic) NSMutableArray *tableArr;
@property (weak, nonatomic) IBOutlet MCPercentageDoughnutView *percentageDoughnut;
@end

@implementation ViewController

- (void)viewDidLoad {
    _tableArr = [[NSMutableArray alloc]init];
    [_tbMain setBackgroundView:nil];
    [_tbMain setBackgroundColor:[UIColor clearColor]];
    [self setData];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)setData
{
    NSString *urlinit=@"http://techartlove.com/api.php";
    [self connetGet:urlinit successCallback:^(id parsedJSON)
     {
         NSDictionary *items = parsedJSON;
         if([items count]==0)
         {
         }
         else
         {
             [self setNavigation:[items objectForKey:@"name"]];
             
             CellData *cellDATA = [[CellData alloc]init];
             cellDATA.name = [items objectForKey:@"name"];
             cellDATA.iconUrl = [items objectForKey:@"image_url"];
             cellDATA.title = [items objectForKey:@"title"];
             
             cellDATA.engagement = [NSString stringWithFormat:@"%@",[items objectForKey:@"engagement"]];
             cellDATA.followers = [NSString stringWithFormat:@"%@",[items objectForKey:@"followers"]];
             cellDATA.expert_in = [NSString stringWithFormat:@"%@",[items objectForKey:@"expert_in"]];
             cellDATA.compatibility = [NSString stringWithFormat:@"%@",[items objectForKey:@"compatibility"]];
             cellDATA.shared = [NSString stringWithFormat:@"%@",[items objectForKey:@"shared"]];
             cellDATA.following = [[items objectForKey:@"following"] boolValue];
             
             NSMutableArray *arrRecently_rated = [[NSMutableArray alloc]init];
             for (int i=1; i<=[[items objectForKey:@"recently_rated"] count]; i++)
             {
                 NSString *result = [NSString stringWithFormat:@"%d",i];
                 
                 CellData *cellDATA2 = [[CellData alloc]init];
                 cellDATA2.iconUrl = [[items objectForKey:@"recently_rated"] objectForKey:result];
                 
                 //[arrRecently_rated setValue:cellDATA2 forKey:result];
                 
                 [arrRecently_rated addObject:[[items objectForKey:@"recently_rated"] objectForKey:result]];
             }
             
             cellDATA.recently_rated = arrRecently_rated;
             
             [_tableArr addObject:cellDATA];
             [self setBanner];
             
             
         }
         
     }
          errorCallback:^(NSError *error,NSString *ErrMSG)
     {
     }];

}
//table main
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int returnNum=50;
    if(indexPath.row==0)
    {
        returnNum = 237;
    }
    else if(indexPath.row==1)
    {
        returnNum = 122;
    }
    else if(indexPath.row==2)
    {
        returnNum = 129;
    }
    else if(indexPath.row==3)
    {
        returnNum = 37;
    }
    else if(indexPath.row==4)
    {
        returnNum = 67;
    }
    else if(indexPath.row==5)
    {
        returnNum = 51;
    }
    return returnNum;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count=0;
    if([_tableArr  count]>0)
    {
        count=6;
    }
    return count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CellData *cellDATA = [_tableArr objectAtIndex:0];
    if(indexPath.row==0)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sec1" forIndexPath:indexPath];
        UILabel *namelb = (UILabel *)[cell viewWithTag:101];
        namelb.text = cellDATA.name;
        
        UILabel *titlelb = (UILabel *)[cell viewWithTag:102];
        titlelb.text = cellDATA.title;
        
        UIImageView *ImageView = (UIImageView *)[cell viewWithTag:100];

        if (cellDATA.icon) {
            [ImageView setImage:cellDATA.icon];
            CALayer *layer = ImageView.layer;
            CGFloat radius = 65.0;
            [layer setMasksToBounds:YES];
            [layer setCornerRadius:radius ];
            [layer setBorderColor:[[UIColor whiteColor] CGColor]];
            [layer setBorderWidth:5.0];
            [layer setBounds:CGRectMake(0.0f, 0.0f, radius *2 ,radius *2)];
        } else {
            [self downloadImageWithURL:[NSURL URLWithString:[cellDATA.iconUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] setIndedxPath:indexPath completionBlock:^(BOOL succeeded, UIImage *image, NSIndexPath *indexP) {
                
                if (succeeded)
                {
                    [self imageDidLoad:indexP setImg:image];
                }
            }];
            
        }
        
        [cell setBackgroundView:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(indexPath.row==1)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sec2" forIndexPath:indexPath];
        UILabel *engagementlb = (UILabel *)[cell viewWithTag:200];
        engagementlb.text = cellDATA.engagement;
        
        UILabel *followerslb = (UILabel *)[cell viewWithTag:201];
        followerslb.text = cellDATA.followers;
        
        UILabel *expert_inlb = (UILabel *)[cell viewWithTag:202];
        expert_inlb.text = cellDATA.expert_in;
        
        [cell setBackgroundView:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(indexPath.row==2)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sec3" forIndexPath:indexPath];
        NSArray* arrCompatibility = [cellDATA.compatibility componentsSeparatedByString: @"."];
        
        UILabel *compatibilityFirst = (UILabel *)[cell viewWithTag:300];
        compatibilityFirst.text = [arrCompatibility objectAtIndex:0];
        
        UILabel *compatibilityLast = (UILabel *)[cell viewWithTag:301];
        compatibilityLast.text = [NSString stringWithFormat:@".%@",[arrCompatibility objectAtIndex:1]];
        
        UILabel *titlelb = (UILabel *)[cell viewWithTag:302];
        titlelb.text = cellDATA.shared;
        
        MCPercentageDoughnutView *percentageDoughnutRow = (MCPercentageDoughnutView *)[cell viewWithTag:310];
        
        percentageDoughnutRow.textStyle               = MCPercentageDoughnutViewTextStyleUserDefined;
        percentageDoughnutRow.percentage              = [cellDATA.compatibility floatValue]/10;
        ;
        percentageDoughnutRow.linePercentage          = 0.3;
        percentageDoughnutRow.animationDuration       = 1;
        percentageDoughnutRow.showTextLabel           = YES;
        percentageDoughnutRow.animatesBegining        = YES;
        //percentageDoughnutRow.textLabel.textColor     = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        //percentageDoughnutRow.textLabel.text          = [NSString stringWithFormat:@"%@", cellDATA.compatibility];
        //percentageDoughnutRow.textLabel.font          = [UIFont italicSystemFontOfSize:10];
        
        
        [cell setBackgroundView:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(indexPath.row==3)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sec4" forIndexPath:indexPath];
        
        UIButton *followingBT = (UIButton *)[cell viewWithTag:400];
        [followingBT addTarget:self action:@selector(toggleFollowing:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell setBackgroundView:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(indexPath.row==4)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sec5" forIndexPath:indexPath];
        
        [cell setBackgroundView:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(indexPath.row==5)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sec6" forIndexPath:indexPath];
        UILabel *namelb = (UILabel *)[cell viewWithTag:101];
        namelb.text = cellDATA.name;
        
        UILabel *titlelb = (UILabel *)[cell viewWithTag:102];
        titlelb.text = cellDATA.title;
        
        UIImageView *ImageView = (UIImageView *)[cell viewWithTag:100];
        
        if (cellDATA.icon) {
            [ImageView setImage:cellDATA.icon];
        } else {
            [self downloadImageWithURL:[NSURL URLWithString:[cellDATA.iconUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] setIndedxPath:indexPath completionBlock:^(BOOL succeeded, UIImage *image, NSIndexPath *indexP) {
                
                if (succeeded)
                {
                    [self imageDidLoad:indexP setImg:image];
                }
            }];
            
        }
        
        [cell setBackgroundView:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sec1" forIndexPath:indexPath];
        UILabel *namelb = (UILabel *)[cell viewWithTag:101];
        namelb.text = cellDATA.name;
        
        UILabel *titlelb = (UILabel *)[cell viewWithTag:102];
        titlelb.text = cellDATA.title;
        
        UIImageView *ImageView = (UIImageView *)[cell viewWithTag:100];
        
        if (cellDATA.icon) {
            [ImageView setImage:cellDATA.icon];
        } else {
            [self downloadImageWithURL:[NSURL URLWithString:[cellDATA.iconUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] setIndedxPath:indexPath completionBlock:^(BOOL succeeded, UIImage *image, NSIndexPath *indexP) {
                
                if (succeeded)
                {
                    [self imageDidLoad:indexP setImg:image];
                }
            }];
            
        }
        
        [cell setBackgroundView:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

-(void)setBanner
{
        CellData *cellDATA = [_tableArr objectAtIndex:0];

         CGSize screenSize = [[UIScreen mainScreen] bounds].size;
         UIScrollView *_scMain=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,screenSize.width ,108)];

    int j=0;

            for (NSString *item in cellDATA.recently_rated)
            {
            NSLog(@"item :: %@",item);
            
             UIButton *imageView=[[UIButton alloc]initWithFrame:CGRectMake(j * 110,
                                                                           0,
                                                                           80 ,
                                                                           108)];
             
             imageView.tag = j;
             //[imageView addTarget:self action:@selector(bannerClicked:) forControlEvents:UIControlEventTouchUpInside];
             //[imageView setBackgroundColor:[UIColor grayColor]];
             
             NSString *imageURL = [NSString stringWithFormat:@"%@",item];
             [self downloadImageWithURL:[NSURL URLWithString:imageURL] completionBlock:^(BOOL succeeded, UIImage *image) {
                 if (succeeded) {
                     
                     [imageView setBackgroundImage:image forState:UIControlStateNormal];
                     //[imageView setImage:image forState:UIControlStateNormal];
                     
                 }
             }];
             
             [_scMain addSubview:imageView];
             
             j++;
             
         }
         _scMain.delegate = self;
         _scMain.pagingEnabled = YES;
         _scMain.contentSize = CGSizeMake([cellDATA.recently_rated count] * 110, 108);
    
         
         
         UIView *mainView=[[UIView alloc]initWithFrame:CGRectMake(0,
                                                                  0,
                                                                  80 ,
                                                                  108)];
         
         [mainView addSubview:_scMain];
         
         [_tbMain setTableFooterView:mainView];
    
    [_tbMain reloadData];

}

- (void)imageDidLoad:(NSIndexPath *)indexPath setImg:(UIImage *)image
{
    @try {
        CellData *cellData = [_tableArr objectAtIndex:0];
        if([_tableArr count]>0)
        {
            @try
            {
                cellData.icon = image;
                [self.tbMain reloadRowsAtIndexPaths:[NSArray arrayWithObject: indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            @catch (NSException *exception) {
            }
        }
    }
    @catch (NSException *exception) {
    }
}


-(IBAction)toggleFollowing:(id)sender
{
    UIButton *senderBt = sender;
    if([senderBt isSelected])
    {
        [senderBt setSelected:NO];
    }
    else
    {
        [senderBt setSelected:YES];
    }
}

-(void)setNavigation:(NSString *)title
{
    self.navigationController.navigationBar.barTintColor = [UIColor yellowColor];
    self.navigationController.navigationBar.translucent = YES;
    
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *lbTitle = [[UILabel alloc]initWithFrame:frame];
    lbTitle.text = title;
    lbTitle.textColor = [UIColor blackColor];
    [lbTitle setFont:[UIFont systemFontOfSize:20]];
    [self.navigationItem setTitleView:lbTitle];

}

-(void)viewDidAppear:(BOOL)animated
{
    self.title = @"";
    [super viewDidAppear:animated];
}

-(void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

-(void)downloadImageWithURL:(NSURL *)url setIndedxPath:(NSIndexPath *)indexPath completionBlock:(void (^)(BOOL succeeded, UIImage *image,NSIndexPath *indexP))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image,indexPath);
                               } else{
                                   completionBlock(NO,nil,nil);
                               }
                           }];
}

- (void) connetGet:(NSString *)urlText successCallback:(void (^)(id jsonResponse)) successCallback
     errorCallback:(void (^)(NSError * error, NSString *errorMsg)) errorCallback
{
    NSString *urlinit = urlText;
    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
    urlinit = [urlinit stringByAddingPercentEncodingWithAllowedCharacters:set];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer =[AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *req = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlinit parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:@"text/html" forHTTPHeaderField:@"Accept"];
    

    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSError *error = nil;
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            //NSLog(@"Reply JSON: %@", jsonArray);
            successCallback(jsonArray);
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                //blah blah
            }
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            errorCallback(error, nil);
        }
    }] resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
