//
//  CellData.h
//  LazyLoadTableImages
//
//  Created by Kshitiz Ghimire on 2/28/11.
//  Copyright 2011 Javra Software. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface CellData : NSObject
{
	NSString *name;
    NSString *iconUrl;
    UIImage *icon;
    NSString *title;
    NSString *engagement;
    NSString *followers;
    NSString *expert_in;
    NSString *compatibility;
    NSString *shared;
	BOOL following;
    NSMutableArray *recently_rated;
}
@property (nonatomic, retain) NSString *name;
@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, retain) NSString *iconUrl;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *engagement;
@property (nonatomic, retain) NSString *followers;
@property (nonatomic, retain) NSString *expert_in;
@property (nonatomic, retain) NSString *compatibility;
@property (nonatomic, retain) NSString *shared;
@property (nonatomic) BOOL following;
@property (nonatomic, retain) NSMutableArray *recently_rated;




@end
