//
//  AppDelegate.h
//  iostest
//
//  Created by katobit on 12/2/2559 BE.
//  Copyright © 2559 katobit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

